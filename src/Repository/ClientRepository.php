<?php

namespace App\Repository;

use App\Entity\Client;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Client::class);
    }

    public function findOneByIDCardOrEmail($IDCard, $email) : ?Client
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.IDCard = :IDCard')
                ->orwhere('a.email = :email')
                ->setParameter('IDCard',$IDCard)
                ->setParameter('email',$email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }


    public function findOneByEmail($email)
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.email = :email')
                ->setParameter('email',$email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByPasswordAndEmail($password, $email)
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.password = :password')
                ->andWhere('a.email = :email')
                ->setParameter('password',$password)
                ->setParameter('email',$email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }


    public function findOneByUid($uid) : ?Client
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.faceBookId = :faceBookId' )
                ->orWhere('a.googleId = :googleId')
                ->orWhere('a.vkId = :vkId')
                ->setParameter('vkId',$uid)
                ->setParameter('faceBookId',$uid)
                ->setParameter('googleId',$uid)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();

        } catch (NonUniqueResultException $e) {
            return null;
        }
    }


}
