<?php

namespace App\Controller;

use App\Entity\Client;
use App\Model\Client\ClientHandler;
use App\Repository\ClientRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{



    /**
     * @Route("/ping", name="app_index")
     */
    public function indexAction()
    {
        return new Response('ping');
    }

    /**
     * @Route("/client/password/encode", name="app_client_password_encode")
     * @param ClientHandler $clientHandler
     * @param Request $request
     * @return JsonResponse
     */
    public function passwordEncodeAction(
        ClientHandler $clientHandler,
        Request $request
    )
    {
        return new JsonResponse(
            [
                'result' => $clientHandler->encodePlainPassword(
                    $request->query->get('plainPassword')
                )
            ]
        );
    }

    /**
     * @Route("/client/{IDCard}/{email}", name="app_client_exist")
     * @Method("HEAD")
     * @param string $IDCard
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return Response
     */
    public function clientExistAction(string $IDCard, string $email, ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByIDCardOrEmail($IDCard, $email)) {

            return new JsonResponse(['result' => 'ok']);

        } else {

            throw   new NotFoundHttpException();

        }
    }


    /**
     * @Route("/client", name="app_create_client")
     * @Method("POST")
     * @param ClientRepository $clientRepository
     * @param Request $request
     * @param ObjectManager $manager
     * @param ClientHandler $clientHandler
     * @return JsonResponse
     */
    public function clientCreateAction(ClientRepository $clientRepository, Request $request,
                                ObjectManager $manager, ClientHandler $clientHandler)
    {
        $data['email'] = $request->request->get('email');
        $data['IDCard']  = $request->request->get('IDCard');
        $data['password']  = $request->request->get('password');
        $data['vkId'] = $request->request->get('vkId');
        $data['faceBookId'] = $request->request->get('faceBookId');
        $data['googleId'] = $request->request->get('googleId');

        if(empty($data['email']) || empty($data['IDCard']) || empty($data['password'])){
            return new JsonResponse(['error' => 'Вы не все поля заполнили'], 406);
        }
        if ($clientRepository->findOneByIDCardOrEmail($data['IDCard'], $data['email'])) {

            return new JsonResponse(['error' => 'Такой клиенте уже существует '], 406);

        } else {
            $client =  $clientHandler->createNewClient($data);
            $manager->persist($client);
            $manager->flush();

            return new JsonResponse(['Клиент успешно создан']);
        }
    }

    /**
     * @Route("/client/email/{email}", name="app_client_by-email")
     * @Method("GET")
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientByEmailAction(string $email, ClientRepository $clientRepository)
    {
        $user =$clientRepository->findOneByEmail($email);

        if($user){
            return new JsonResponse($user->__toArray());
        }else{
            throw new NotFoundHttpException();
        }

    }

    /**
     * @Route("/check_client/uid/{uid}", name="app_client_by_uid")
     * @Method("HEAD")
     * @param string $uid
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientByUidAction(string $uid, ClientRepository $clientRepository)
    {
        $user =$clientRepository->findOneByUid($uid);

        if($user){
            return new JsonResponse(['result' => 'ok']);
        }else{
            throw new NotFoundHttpException();
        }

    }

    /**
     * @Route("/check_client_credentials/{encodedPassword}/{email}", name="check_client_credentials")
     * @Method("HEAD")
     * @param string $encodedPassword
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return Response
     */
    public function checkClientCredentialsAction(string $encodedPassword, string $email, ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByPasswordAndEmail($encodedPassword, $email)) {

            return new JsonResponse(['result' => 'ok']);

        } else {

            throw   new NotFoundHttpException();

        }
    }

    /**
     * @Route("/check_client/uid/{uid}", name="app_client_by_uid")
     * @Method("GET")
     * @param string $uid
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function getClientByUidAction(string $uid, ClientRepository $clientRepository)
    {
        $user =$clientRepository->findOneByUid($uid);

        if($user){
            return new JsonResponse($user->__toArray());
        }else{
            throw new NotFoundHttpException();
        }

    }

    /**
     * @Route("/bind_social/email/{email}/IDCard/{IDCard}/uid/{uid}/network/{network}", name="app_bind_social")
     * @Method("GET")
     * @param string $email
     * @param string $IDCard
     * @param string $uid
     * @param string $network
     * @param ClientRepository $clientRepository
     * @param ObjectManager $manager
     * @return JsonResponse
     */
    public function bindSocialNetworkAction(string $email ,string $IDCard
        , string $uid , string $network ,ClientRepository $clientRepository, ObjectManager $manager)
    {
        $client = $clientRepository->findOneByIDCardOrEmail($IDCard, $email);

        switch ($network){
            case ClientHandler::SOC_NETWORK_VKONTAKTE:
                $client->setVkId($uid);
                $manager->persist($client);
                break;
            case ClientHandler::SOC_NETWORK_FACEBOOK:
                $client->setFaceBookId($uid);
                $manager->persist($client);
                break;
            case ClientHandler::SOC_NETWORK_GOOGLE:
                $client->setGoogleId($uid);
                $manager->persist($client);
                break;
        }
        $manager->flush();

        return new JsonResponse($client->__toArray(),200);
    }


}
