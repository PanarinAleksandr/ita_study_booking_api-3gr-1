<?php

namespace App\DataFixtures;

use App\Model\Client\ClientHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class ClientFixtures extends Fixture
{
    /**
     * @var ClientHandler
     */
    private $clientHandler;

    public function __construct(ClientHandler $clientHandler)
    {
        $this->clientHandler = $clientHandler;
    }

    public function load(ObjectManager $manager)
    {
             $client = $this->clientHandler->createNewClient([
                 'email' => '123@123.ru',
                 'IDCard' => 'qwert & qwert',
                 'password' =>  '123321'
    ]);
             $manager->persist($client);

        $user1 = $this->clientHandler->createNewClient([
            'email' => '321@321.ru',
            'IDCard' => 'qwert & qwert',
            'password' => '321123'
        ]);
        $manager->persist($user1);


             $manager->flush();
    }
}