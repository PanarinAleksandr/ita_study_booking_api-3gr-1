<?php

namespace App\Security\PreAuthenticatedToken;


use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;

class AnonymousPreAuthenticatedToken extends PreAuthenticatedToken
{
    public function __construct(string $providerKey)
    {
        $user = new User('ANONYMOUS', null, ['ROLE_ANONYMOUS']);
        parent::__construct($user, null, $providerKey, ['ROLE_ANONYMOUS']);
    }
}
