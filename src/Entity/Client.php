<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 * @ORM\Table(name="Client")
 */

class Client
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(name="email")
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(type="string", name="IDCard")
     */
    private $IDCard;


    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",name="date" )
     */
    private $date;

    /**
     * @ORM\Column(type="string", name="password")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $vkId;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $faceBookId;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $googleId;

    public function __construct()
    {
        $this->date = new \DateTime("now");
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param mixed $email
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }



    /**
     * @param mixed $password
     * @return Client
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $IDCard
     * @return Client
     */
    public function setIDCard(string $IDCard): Client
    {
        $this->IDCard = $IDCard;
        return $this;
    }

    /**
     * @return string
     */
    public function getIDCard(): string
    {
        return $this->IDCard;
    }
    public function __toArray() {

        return [
            'email' => $this->email,
            'password' => $this->password,
            'IDCard' => $this->IDCard,
            'vkId' => $this->vkId,
            'faceBookId' => $this->faceBookId,
            'googleId' => $this->googleId
        ];
    }

    /**
     * @param mixed $vkId
     * @return Client
     */
    public function setVkId($vkId)
    {
        $this->vkId = $vkId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVkId()
    {
        return $this->vkId;
    }

    /**
     * @param mixed $faceBookId
     * @return Client
     */
    public function setFaceBookId($faceBookId)
    {
        $this->faceBookId = $faceBookId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFaceBookId()
    {
        return $this->faceBookId;
    }

    /**
     * @param mixed $googleId
     * @return Client
     */
    public function setGoogleId($googleId)
    {
        $this->googleId = $googleId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGoogleId()
    {
        return $this->googleId;
    }


}